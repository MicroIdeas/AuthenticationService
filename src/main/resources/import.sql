-- You can use this file to load seed data into the database using SQL statements
insert into Member (id, name, email, phone_number) values (0, 'John Smith', 'john.smith@mailinator.com', '2125551212') 

insert into rest_user (uuid, first_name,	last_name,	email_address, hashed_password, is_verified, time_created, version, role) values ('ab781ea7-594a-443f-8f6f-f21e88ef6c66','System', 'Administrator', 'admin@admin.com','EpyrgXZaDxmaQv+pW0AOCaKbJnZTxx1fP+AXARUUJDs=', 1, current_date, 0, 'administrator')
insert into rest_user (uuid, first_name,	last_name,	email_address, hashed_password, is_verified, time_created, version, role) values ('ab781ea7-594a-443f-8f6f-f21e88ef6c67','Eduardo', 'Rodriguez', 'sinapsis490@gmail.com','gZmkslCQ/nh6H4ekHt+TH54SRUJyn+k7xFik/lyoB54=', 1, current_date, 0, 'administrator')
insert into rest_user (uuid, first_name,	last_name,	email_address, hashed_password, is_verified, time_created, version, role) values ('ab781ea7-594a-443f-8f6f-f21e88ef6c69','John', 'Doe', 'johndoe@gmail.com','8tALVNGn1m7LlbYL/ZRFMekN8Pw6JPcUECFfa+bm5e4=', 1, current_date, 0, 'authenticated')

insert into rest_authorization_token (token, user_id, time_created, last_updated, expiration_date) values ('d3315bf5-a50b-45de-92f9-1a4745846e96', 'ab781ea7-594a-443f-8f6f-f21e88ef6c67', CURRENT_TIME-1 DAY  , CURRENT_TIME, CURRENT_TIME+30 DAY )


--current_date
