package ar.com.leadprogrammer.framework.user.domain.service;


import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import ar.com.leadprogrammer.framework.configuration.ApplicationConfig;
import ar.com.leadprogrammer.framework.encryption.Base64Converter;
import ar.com.leadprogrammer.framework.rest.autentication.exception.AlreadyVerifiedException;
import ar.com.leadprogrammer.framework.rest.autentication.exception.AuthenticationException;
import ar.com.leadprogrammer.framework.rest.autentication.exception.TokenHasExpiredException;
import ar.com.leadprogrammer.framework.rest.autentication.exception.TokenNotFoundException;
import ar.com.leadprogrammer.framework.rest.autentication.exception.UserNotFoundException;
import ar.com.leadprogrammer.framework.user.api.model.EmailServiceTokenModel;
import ar.com.leadprogrammer.framework.user.api.model.Role;
import ar.com.leadprogrammer.framework.user.api.operations.LostPasswordRequest;
import ar.com.leadprogrammer.framework.user.api.operations.PasswordRequest;
import ar.com.leadprogrammer.framework.user.domain.entities.User;
import ar.com.leadprogrammer.framework.user.domain.entities.VerificationToken;
import ar.com.leadprogrammer.framework.util.StringUtil;

/**
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 10/09/2012
 */
@Stateless
@Repository
@Component
public class VerificationTokenService {



    @Autowired
    private UserService userService;
    
    @Autowired
    private EmailServicesGateway emailServicesGateway;

	@Inject
	private EntityManager em;


	@Autowired
    ApplicationConfig config;



    public VerificationToken sendEmailVerificationToken(String userId) {
        User user = ensureUserIsLoaded(userId);
        return sendEmailVerificationToken(user);
    }

    private VerificationToken sendEmailVerificationToken(User user) {
        VerificationToken token = new VerificationToken(user, VerificationToken.VerificationTokenType.emailVerification,
                config.getEmailVerificationTokenExpiryTimeInMinutes());
        user.addVerificationToken(token);
        userService.save(user);
        emailServicesGateway.sendVerificationToken(new EmailServiceTokenModel(user, token, config.getHostNameUrl()));
        return token;
    }

    public VerificationToken sendEmailRegistrationToken(String userId) {
        User user = ensureUserIsLoaded(userId);
        VerificationToken token = new VerificationToken(user,
                VerificationToken.VerificationTokenType.emailRegistration,
                config.getEmailRegistrationTokenExpiryTimeInMinutes());
        user.addVerificationToken(token);
        userService.save(user);
        emailServicesGateway.sendVerificationToken(new EmailServiceTokenModel(user,
                token,config.getHostNameUrl()));
        return token;
    }

    /**
     * generate token if user found otherwise do nothing
     *
     * @param lostPasswordRequest
     * @return  a token or null if user not found
     */

    public VerificationToken sendLostPasswordToken(LostPasswordRequest lostPasswordRequest) {
        //validate(lostPasswordRequest);
        VerificationToken token = null;
        User user = userService.findByEmailAddress(lostPasswordRequest.getEmailAddress());
        if (user != null) {
            token = user.getActiveLostPasswordToken();
            if (token == null) {
                token = new VerificationToken(user, VerificationToken.VerificationTokenType.lostPassword,
                        config.getLostPasswordTokenExpiryTimeInMinutes());
                user.addVerificationToken(token);
                userService.save(user);
            }
            emailServicesGateway.sendVerificationToken(new EmailServiceTokenModel(user, token,config.getHostNameUrl()));
        }

        return token;
    }

    public VerificationToken verify(String base64EncodedToken) {
        VerificationToken token = loadToken(base64EncodedToken);
        if (token.isVerified() || token.getUser().isVerified()) {
            throw new AlreadyVerifiedException();
        }
        token.setVerified(true);
        token.getUser().setVerified(true);
        userService.save(token.getUser());
        return token;
    }

    public VerificationToken generateEmailVerificationToken(String emailAddress) {
        Assert.assertNotNull(emailAddress);
        User user = userService.findByEmailAddress(emailAddress);
        if (user == null) {
            throw new UserNotFoundException();
        }
        if (user.isVerified()) {
            throw new AlreadyVerifiedException();
        }
        //if token still active resend that
        VerificationToken token = user.getActiveEmailVerificationToken();
        if (token == null) {
            token = sendEmailVerificationToken(user);
        } else {
            emailServicesGateway.sendVerificationToken(new EmailServiceTokenModel(user, token, config.getHostNameUrl()));
        }
        return token;
    }

    public VerificationToken resetPassword(String base64EncodedToken, PasswordRequest passwordRequest) {
        Assert.assertNotNull(base64EncodedToken);
        //validate(passwordRequest);
        VerificationToken token = loadToken(base64EncodedToken);
        if (token.isVerified()) {
            throw new AlreadyVerifiedException();
        }
        token.setVerified(true);
        User user = token.getUser();
        try {
            user.setHashedPassword(user.hashPassword(passwordRequest.getPassword()));
        } catch (Exception e) {
            throw new AuthenticationException();
        }
        //set user to verified if not already and authenticated role
        user.setVerified(true);
        if (user.hasRole(Role.anonymous)) {
            user.setRole(Role.authenticated);
        }
        userService.save(user);
        return token;
    }

    private VerificationToken loadToken(String base64EncodedToken) {
        Assert.assertNotNull(base64EncodedToken);
        String rawToken = Base64Converter.convertBase64RepresentationToString(base64EncodedToken);
        VerificationToken token = findByToken(rawToken);
        if (token == null) {
            throw new TokenNotFoundException();
        }
        if (token.hasExpired()) {
            throw new TokenHasExpiredException();
        }
        return token;
    }

    private User ensureUserIsLoaded(String userIdentifier) {
        User user = null;
        if (StringUtil.isValidUuid(userIdentifier)) {
            user = userService.findByUuid(userIdentifier);
        } else {
            user = userService.findByEmailAddress(userIdentifier);
        }
        if (user == null) {
            throw new UserNotFoundException();
        }
        return user;
    }


	
	public VerificationToken findByUuid(String uuid) {
		
		String query = "select t from VerificationToken t where uuid = ?";
		return (VerificationToken) em.createQuery(query).setParameter(1, uuid)
				.getSingleResult();

	}

	
	public VerificationToken findByToken(String token) {
		String query = "select t from VerificationToken t where token = ?";
		return (VerificationToken) em.createQuery(query).setParameter(1, token)
				.getSingleResult();
	}
}
