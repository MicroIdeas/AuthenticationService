package ar.com.leadprogrammer.framework.user.api.connection;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import ar.com.leadprogrammer.framework.user.api.model.Role;
import ar.com.leadprogrammer.framework.user.api.model.social.UserProfile;
import ar.com.leadprogrammer.framework.user.domain.entities.User;
import ar.com.leadprogrammer.framework.user.domain.service.UserService;

public class UsersConnectionRepository {

	@Inject
	private Logger log;

	@Inject
	protected UserService userService;

	public UsersConnectionRepository(UserService userService) {
		this.userService = userService;
	}

	/**
	 * Find User with the Connection profile (providerId and providerUserId) If
	 * this is the first connection attempt there will be nor User so create one
	 * and persist the Connection information In reality there will only be one
	 * User associated with the Connection
	 * 
	 * @param connection
	 * @return List of User Ids (see User.getUuid())
	 */
	public List<String> findUserIdsWithConnection(Connection<?> connection) {

		List<String> userIds = new ArrayList<String>();

		UserProfile userProfile = connection.fetchUserProfile();

		User user = userService.findByEmailAddress(userProfile.getEmail());

		if (user != null) {

			log.info("User Found");
			userIds.add(user.getUuid().toString());
			

		} 
		return userIds;
	}

}