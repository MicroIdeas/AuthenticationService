package ar.com.leadprogrammer.framework.user.api.connection;

import ar.com.leadprogrammer.framework.user.api.model.social.UserProfile;


public interface Connection<T> {

	public UserProfile fetchUserProfile();
	
	
}
