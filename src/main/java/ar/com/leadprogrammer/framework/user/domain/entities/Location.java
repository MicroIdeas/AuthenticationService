package ar.com.leadprogrammer.framework.user.domain.entities;

import javax.persistence.Entity;

import ar.com.leadprogrammer.framework.user.api.model.ExternalLocation;

@Entity
public class Location extends BaseEntity {

	private Double latitude;
	
	private Double longitude;
	
	private String address;

	public Location() {
		super();
	}
	
	public Location(ExternalLocation location) {
		this.latitude = location.getLatitude();
		this.longitude = location.getLongitude();
		this.address = location.getAddress();
	}

	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	
}
