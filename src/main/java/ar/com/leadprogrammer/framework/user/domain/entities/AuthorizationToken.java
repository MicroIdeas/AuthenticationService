package ar.com.leadprogrammer.framework.user.domain.entities;


import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @version 1.0
 * @author: Iain Porter iain.porter
 * @since 28/12/2012
 */
@Entity
@Table(name="rest_authorization_token")
public class AuthorizationToken implements Comparable<AuthorizationToken>  {

    private final static Integer DEFAULT_TIME_TO_LIVE_IN_SECONDS = (60 * 60 * 24 * 30); //30 Days

	@Id
    @Column(length=36)
    private String token;

    private Date timeCreated;

    private Date lastUpdated;

    private Date expirationDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    public AuthorizationToken() {}

    public AuthorizationToken(User user) {
        this(user, DEFAULT_TIME_TO_LIVE_IN_SECONDS);
    }

    public AuthorizationToken(User user, Integer timeToLiveInSeconds) {
        this.token = UUID.randomUUID().toString();
        this.user = user;
        this.timeCreated = new Date();
        this.lastUpdated = new Date();
        this.expirationDate = new Date(System.currentTimeMillis() + (timeToLiveInSeconds * 1000L));
    }


    
    public String getToken() {
        return token;
    }

    public User getUser() {
        return user;
    }

    public Date getTimeCreated() {
        return timeCreated;
    }

    public int compareTo(AuthorizationToken userSession) {
        return this.lastUpdated.compareTo(userSession.getLastUpdated());
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    public boolean hasExpired() {
        return this.expirationDate != null && this.expirationDate.before(new Date());
    }

}
