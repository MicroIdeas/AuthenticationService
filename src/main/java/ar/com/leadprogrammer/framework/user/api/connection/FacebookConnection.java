package ar.com.leadprogrammer.framework.user.api.connection;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.User;
import ar.com.leadprogrammer.framework.user.api.model.social.UserProfile;

public class FacebookConnection<T> implements Connection<T> {

	
	private Facebook facebook;

	FacebookConnection(){
		
	}
	
	public FacebookConnection(Facebook facebook) {
		this.facebook = facebook;
	}

	@Override
	public UserProfile fetchUserProfile() {
		
		UserProfile profile = null;
		try {
			User user =this.facebook.getMe();
			
			profile = new UserProfile();
			profile.setFirstName(user.getFirstName());
			profile.setMail(user.getEmail());
			profile.setLastName(user.getLastName());
			
		} catch (FacebookException e) {
			e.printStackTrace();
		}
		
		return profile;
	}

}
