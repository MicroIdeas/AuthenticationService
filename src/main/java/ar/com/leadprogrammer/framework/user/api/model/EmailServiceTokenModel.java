package ar.com.leadprogrammer.framework.user.api.model;


import java.io.Serializable;

import ar.com.leadprogrammer.framework.encryption.Base64Converter;
import ar.com.leadprogrammer.framework.user.domain.entities.User;
import ar.com.leadprogrammer.framework.user.domain.entities.VerificationToken;

/**
 *
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 13/09/2012
 */
public class EmailServiceTokenModel implements Serializable {

	private static final long serialVersionUID = -780025790650951410L;
	private final String emailAddress;
    private final String token;
    private final VerificationToken.VerificationTokenType tokenType;
    private final String hostNameUrl;


    public EmailServiceTokenModel(User user, VerificationToken token, String hostNameUrl)  {
        this.emailAddress = user.getEmailAddress();
        this.token = token.getToken();
        this.tokenType = token.getTokenType();
        this.hostNameUrl = hostNameUrl;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getEncodedToken() {
    	
    	return Base64Converter.convertStringToBase64Representation(token);
    }

    public String getToken() {
        return token;
    }

    public VerificationToken.VerificationTokenType getTokenType() {
        return tokenType;
    }

    public String getHostNameUrl() {
        return hostNameUrl;
    }
}
