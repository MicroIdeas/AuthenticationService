package ar.com.leadprogrammer.framework.user.api.operations;


import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import ar.com.leadprogrammer.framework.validations.annotations.Email;

/**
 *
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 05/10/2012
 */
@XmlRootElement
public class UpdateUserRequest {

    private String firstName;
    private String lastName;

    @Email
    @NotNull
    private String emailAddress;

    public UpdateUserRequest(){}

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
