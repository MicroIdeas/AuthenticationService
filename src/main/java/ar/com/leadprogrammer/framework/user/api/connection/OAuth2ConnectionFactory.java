package ar.com.leadprogrammer.framework.user.api.connection;

import ar.com.leadprogrammer.framework.user.api.model.social.AccessGrant;

public interface OAuth2ConnectionFactory<T> {

	public Connection<?> createConnection(AccessGrant accessGrant);
}

