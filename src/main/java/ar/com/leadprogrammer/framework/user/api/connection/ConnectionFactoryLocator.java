package ar.com.leadprogrammer.framework.user.api.connection;

public class ConnectionFactoryLocator {

	public OAuth2ConnectionFactory<?> getConnectionFactory(String providerId) {
		
		OAuth2ConnectionFactory<?> connection=null;
		switch (providerId){
		case "facebook":{
			
			connection = new FacebookConnectionFactory();
			
			break;

		}
		case "twitter":{
			break;

		}
		case "google":{
			break;

		}
		case "yahoo":{
			
			break;
		}

		}
		
		return connection;
	}

}
