package ar.com.leadprogrammer.framework.user.api.connection;

import org.junit.Test;

import facebook4j.Facebook;
import facebook4j.FacebookFactory;
import facebook4j.auth.AccessToken;
import ar.com.leadprogrammer.framework.user.api.model.social.AccessGrant;
import ar.com.leadprogrammer.framework.user.api.model.social.UserProfile;

public class FacebookConnectionFactory<T> implements OAuth2ConnectionFactory<T> {

	private static final String APP_ID = "564140167034984";
	private static final String APP_SECRET="724ec8e5e898c199b536e5283c071600";
	@Override
	public Connection<?> createConnection(AccessGrant accessGrant) {

		Facebook facebook = new FacebookFactory().getInstance();
		facebook.setOAuthAppId(APP_ID, APP_SECRET);
		facebook.setOAuthPermissions("");
		facebook.setOAuthAccessToken(new AccessToken(accessGrant.getAccessToken(), null));
		
		return new FacebookConnection<T> (facebook);
	}

	
	
	@Test
	public void faceConnection(){
		
		String tokenId= "CAAIBFR5CeGgBABTsE3BkYTnOZAyEde5ZCmd2SBrerB1swAjHTGhFW5WVKOy0I1GkZBiax5KfG8meYVsgyES7as95xVdKcHnKiUmCOZCpzX3P7Y674kRybAJFTOb2bADXZAw6y5xZCEby7DrR8P7A7SEnV8Cmf34VS7wFo22PD2bTfOfR1yQ5WI0lBAAYKPOtIZD";
		
		Connection<?> connecy = createConnection(new AccessGrant(tokenId));
		
		
		
		UserProfile userProfile = connecy.fetchUserProfile();
		
		System.out.println(userProfile.getEmail());
		
		
		
	}
	
}
