package ar.com.leadprogrammer.framework.user.domain.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ar.com.leadprogrammer.framework.resources.ResourcePublisher;
import ar.com.leadprogrammer.framework.rest.autentication.exception.AuthenticationException;
import ar.com.leadprogrammer.framework.rest.autentication.exception.AuthorizationException;
import ar.com.leadprogrammer.framework.rest.autentication.exception.DuplicateUserException;
import ar.com.leadprogrammer.framework.rest.autentication.exception.UserNotFoundException;
import ar.com.leadprogrammer.framework.user.api.connection.Connection;
import ar.com.leadprogrammer.framework.user.api.model.AuthenticatedUserToken;
import ar.com.leadprogrammer.framework.user.api.model.ExternalUser;
import ar.com.leadprogrammer.framework.user.api.model.Role;
import ar.com.leadprogrammer.framework.user.api.model.social.UserProfile;
import ar.com.leadprogrammer.framework.user.api.operations.CreateUserRequest;
import ar.com.leadprogrammer.framework.user.api.operations.LoginRequest;
import ar.com.leadprogrammer.framework.user.api.operations.UpdateUserRequest;
import ar.com.leadprogrammer.framework.user.domain.entities.User;
import ar.com.leadprogrammer.framework.util.StringUtil;

@Stateless
@Repository
@Component
@Transactional
public class UserService {

	//@Inject
	//private Logger log;
	
	private static final Logger log = Logger
			.getLogger(UserService.class.getName());

	@PersistenceContext
	protected EntityManager em;


	public User findByUuid(String userId) {
		return em.find(User.class, userId);
	}

	public List<ExternalUser> queryFindAll() {
		List<ExternalUser> usuarios = new ArrayList<>();

		String query = "select u from User u";
		List<User> internalUsers = em.createQuery(query).getResultList();

		for (User currentUser : internalUsers) {

			usuarios.add(new ExternalUser(currentUser));
		}
		
		return usuarios;
	}

	private List<User> findByExpiredSession(Date date) {
		String query = "select u from User u where u in (select user from SessionToken where lastUpdated < ?)";
		return em.createQuery(query).setParameter(1, date).getResultList();
	}

	
	public void delete(User userToDelete) {
			log.info("Entering delete user");
			em.remove(userToDelete);

	}

	public void save(User user) {
		log.info("Entering User");
		em.merge(user);

	}

	public void save(List<User> user) {
		em.merge(user);
	}

	public User findBySession(String token) {
		String query = "select u from User u where u = (select user from AuthorizationToken where token = ?)";
		User user = null;
		try {

			user = (User) em.createQuery(query).setParameter(1, token)
					.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}
		return user;

	}

	public User findByEmailAddress(String emailAddress) {
		String query = "select u from User u where u.emailAddress =  ?";
		User user = null;
		Query statement = em.createQuery(query).setParameter(1, emailAddress);

		try {
			user = (User) statement.getSingleResult();
		} catch (NoResultException e) {
			user = null;
		}
		return user;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method creates a User with the given Role. A check is made to see if
	 * the username already exists and a duplication check is made on the email
	 * address if it is present in the request.
	 * <P>
	 * </P>
	 * The password is hashed and a SessionToken generated for subsequent
	 * authorization of role-protected requests.
	 * 
	 */
	public AuthenticatedUserToken createUser(CreateUserRequest request,
			Role role) {
		// validate(request);
		User searchedForUser = findByEmailAddress(request.getUser()
				.getEmailAddress());
		if (searchedForUser != null) {
			throw new DuplicateUserException();
		}

		User newUser = createNewUser(request, role);
		AuthenticatedUserToken token = new AuthenticatedUserToken(newUser
				.getUuid().toString(), newUser.addSessionToken().getToken());
		save(newUser);
		return token;
	}

	public AuthenticatedUserToken createUser(Role role) {
		User user = new User();
		user.setRole(role);
		AuthenticatedUserToken token = new AuthenticatedUserToken(user
				.getUuid().toString(), user.addSessionToken().getToken());
		save(user);
		return token;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Login supports authentication against an email attribute. If a User is
	 * retrieved that matches, the password in the request is hashed and
	 * compared to the persisted password for the User account.
	 */
	public AuthenticatedUserToken login(LoginRequest request) {
		// validate(request);
		User user = null;
		user = findByEmailAddress(request.getUsername());
		if (user == null) {
			throw new AuthenticationException();
		}
		String hashedPassword = null;
		try {
			hashedPassword = user.hashPassword(request.getPassword());
		} catch (Exception e) {
			throw new AuthenticationException();
		}
		if (hashedPassword.equals(user.getHashedPassword())) {
			String token = user.addSessionToken().getToken();
			
			save(user);

			return new AuthenticatedUserToken(user.getUuid().toString(),token);
		} else {
			throw new AuthenticationException();
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Associate a Connection with a User account. If one does not exist a new
	 * User is created and linked to the
	 * {@link com.porterhead.rest.user.domain.SocialUser} represented in the
	 * Connection details.
	 * 
	 * <P>
	 * </P>
	 * 
	 * A SessionToken is generated and any Profile data that can be collected
	 * from the Social account is propagated to the User object.
	 * 
	 */

	public AuthenticatedUserToken socialLogin(Connection<?> connection) {
		//UsersConnectionRepository jpaUsersConnectionRepository = new UsersConnectionRepository(this);
		List<String> userUuids = findUserIdsWithConnection(connection);
		if (userUuids.size() == 0) {
			throw new AuthenticationException();
		}
		User user = findByUuid(userUuids.get(0)); // take the
													// first one
													// if there
													// are
													// multiple
													// userIds
													// for this
													// provider
													// Connection
		if (user == null) {
			throw new AuthenticationException();
		}
		
		String token = user.addSessionToken().getToken();
		save(user);

		updateUserFromProfile(connection, user);
		
		
		return new AuthenticatedUserToken(user.getUuid().toString(), token);
	}
	
	public List<String> findUserIdsWithConnection(Connection<?> connection) {

		List<String> userIds = new ArrayList<String>();

		UserProfile userProfile = connection.fetchUserProfile();

		User user = findByEmailAddress(userProfile.getEmail());

		if (user != null) {

			log.info("User Found");
			userIds.add(user.getUuid().toString());
			

		} 
		return userIds;
	}

	
	
	private void updateUserFromProfile(Connection<?> connection, User user) {
		UserProfile profile = connection.fetchUserProfile();
		user.setEmailAddress(profile.getEmail());
		user.setFirstName(profile.getFirstName());
		user.setLastName(profile.getLastName());
		// users logging in from social network are already verified
		user.setVerified(true);
		if (user.hasRole(Role.anonymous)) {
			user.setRole(Role.authenticated);
		}
		save(user);
	}


	/**
	 * Allow user to get their own profile or a user with administrator role to
	 * get any profile
	 * 
	 * @param requestingUser
	 * @param userIdentifier
	 * @return user
	 */
	public ExternalUser getUser(ExternalUser requestingUser,
			String userIdentifier) {
		Assert.assertNotNull(requestingUser);
		Assert.assertNotNull(userIdentifier);
		User user = ensureUserIsLoaded(userIdentifier);
		if (!requestingUser.getId().equals(user.getUuid().toString())
				&& !requestingUser.getRole().equalsIgnoreCase(
						Role.administrator.toString())) {
			throw new AuthorizationException(
					"User not authorized to load profile");
		}
		return new ExternalUser(user);
	}

	public void deleteUser(ExternalUser userMakingRequest, String userId) {
		Assert.assertNotNull(userMakingRequest);
		Assert.assertNotNull(userId);
		User userToDelete = ensureUserIsLoaded(userId);
		if (userMakingRequest.getRole().equalsIgnoreCase(
				Role.administrator.toString())
				&& (userToDelete.hasRole(Role.anonymous) || userToDelete
						.hasRole(Role.authenticated))) {
			delete(userToDelete);
		} else {
			throw new AuthorizationException(
					"User cannot be deleted. Only users with anonymous or authenticated role can be deleted.");
		}
	}

	public ExternalUser saveUser(String userId, UpdateUserRequest request) {
		// validate(request);
		User user = ensureUserIsLoaded(userId);
		if (request.getFirstName() != null) {
			user.setFirstName(request.getFirstName());
		}
		if (request.getLastName() != null) {
			user.setLastName(request.getLastName());
		}
		if (request.getEmailAddress() != null) {
			if (!request.getEmailAddress().equals(user.getEmailAddress())) {
				user.setEmailAddress(request.getEmailAddress());
				user.setVerified(false);
			}
		}
		save(user);
		return new ExternalUser(user);
	}

	public Integer deleteExpiredSessions(int timeSinceLastUpdatedInMinutes) {
		DateTime date = new DateTime();
		date = date.minusMinutes(timeSinceLastUpdatedInMinutes);
		List<User> expiredUserSessions = findByExpiredSession(date.toDate());
		int count = expiredUserSessions.size();
		for (User user : expiredUserSessions) {
			user.removeExpiredSessions(date.toDate());
		}
		if (count > 0) {
			save(expiredUserSessions);
		}
		return count;
	}

	private User createNewUser(CreateUserRequest request, Role role) {
		User userToSave = new User(request.getUser());
		try {
			userToSave.setHashedPassword(userToSave.hashPassword(request
					.getPassword().getPassword()));
		} catch (Exception e) {
			e.printStackTrace();
			throw new AuthenticationException();
		}
		userToSave.setRole(role);
		return userToSave;
	}


	private User ensureUserIsLoaded(String userIdentifier) {
		User user = null;
		if (StringUtil.isValidUuid(userIdentifier)) {
			user = findByUuid(userIdentifier);
		} else {
			user = findByEmailAddress(userIdentifier);
		}
		if (user == null) {
			throw new UserNotFoundException();
		}
		return user;
	}

}
