package ar.com.leadprogrammer.framework.user.domain.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ar.com.leadprogrammer.framework.encryption.HashString;
import ar.com.leadprogrammer.framework.rest.autentication.UserSession;
import ar.com.leadprogrammer.framework.user.api.model.ExternalUser;
import ar.com.leadprogrammer.framework.user.api.model.Role;

/**
 * User: porter Date: 09/03/2012 Time: 18:56
 */
@Entity
@Table(name = "rest_user")
public class User extends BaseEntity {

	/**
	 * Add additional salt to password hashing
	 */
	private static final String HASH_SALT = "d8a8e885-ecce-42bb-8332-894f20f0d8ed";

	private static final int HASH_ITERATIONS = 1000;

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String hashedPassword;
	private int isVerified;

	@Enumerated(EnumType.STRING)
	private Role role;

	@OneToMany( mappedBy = "user", cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
	private Set<SocialUser> socialUsers = new HashSet<SocialUser>();

	@OneToMany(mappedBy = "user", targetEntity = VerificationToken.class, cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	private List<VerificationToken> verificationTokens = new ArrayList<VerificationToken>();

	@OneToMany(mappedBy = "user", targetEntity = AuthorizationToken.class, cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<AuthorizationToken> sessions = Collections.synchronizedSortedSet(new TreeSet<AuthorizationToken>(Collections.<AuthorizationToken> reverseOrder()));

	
	public User() {
		super();
	}

	public User(UUID uuid) {
		super(uuid);
		setRole(Role.anonymous); // all users are anonymous until credentials
									// are proved
	}

	public User(ExternalUser externalUser) {
		super();
		this.firstName = externalUser.getFirstName();
		this.lastName = externalUser.getLastName();
		this.emailAddress = externalUser.getEmailAddress();
		if (externalUser.getId() !=null ) 
		this.setUuid(externalUser.getId());
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public String getHashedPassword() {
		return this.hashedPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public boolean hasRole(Role role) {
		return role.equals(this.role);
	}

	public boolean equals(Object otherUser) {
		boolean response = false;

		if (otherUser == null) {
			response = false;
		} else if (!(otherUser instanceof User)) {
			response = false;
		} else {
			if (((User) otherUser).getUuid().equals(this.getUuid())) {
				response = true;
			}
		}

		return response;
	}

	public int hashCode() {
		return getUuid().hashCode();
	}

	public String getName() {
		if (getFirstName() != null && !getFirstName().isEmpty()) {
			return getFirstName() + " " + getLastName();
		}
		return "";
	}

	public Set<SocialUser> getSocialUsers() {
		return socialUsers;
	}

	public void setSocialUsers(Set<SocialUser> socialUsers) {
		this.socialUsers = socialUsers;
	}

	public void addSocialUser(SocialUser socialUser) {
		getSocialUsers().add(socialUser);
	}

	public synchronized void addVerificationToken(VerificationToken token) {
		verificationTokens.add(token);
	}

	public synchronized List<VerificationToken> getVerificationTokens() {
		return Collections.unmodifiableList(this.verificationTokens);
	}

	public AuthorizationToken addSessionToken() {
		AuthorizationToken token = new AuthorizationToken(this);
		this.sessions.add(token);
		return token;
	}

	public SortedSet<AuthorizationToken> getSessions() {
		SortedSet copySet = new TreeSet<AuthorizationToken>(
				Collections.<AuthorizationToken> reverseOrder());
		copySet.addAll(this.sessions);
		return Collections.unmodifiableSortedSet(copySet);
	}

	public void removeSession(AuthorizationToken session) {
		this.sessions.remove(session);
	}

	/**
	 * If the user has a VerificationToken of type
	 * VerificationTokenType.lostPassword that is active return it otherwise
	 * return null
	 * 
	 * @return verificationToken
	 */
	public VerificationToken getActiveLostPasswordToken() {
		return getActiveToken(VerificationToken.VerificationTokenType.lostPassword);
	}

	/**
	 * If the user has a VerificationToken of type
	 * VerificationTokenType.emailVerification that is active return it
	 * otherwise return null
	 * 
	 * @return verificationToken
	 */
	public VerificationToken getActiveEmailVerificationToken() {
		return getActiveToken(VerificationToken.VerificationTokenType.emailVerification);
	}

	/**
	 * If the user has a VerificationToken of type
	 * VerificationTokenType.emailRegistration that is active return it
	 * otherwise return null
	 * 
	 * @return verificationToken
	 */
	public VerificationToken getActiveEmailRegistrationToken() {
		return getActiveToken(VerificationToken.VerificationTokenType.emailRegistration);
	}

	private VerificationToken getActiveToken(
			VerificationToken.VerificationTokenType tokenType) {
		VerificationToken activeToken = null;
		for (VerificationToken token : getVerificationTokens()) {
			if (token.getTokenType().equals(tokenType) && !token.hasExpired()
					&& !token.isVerified()) {
				activeToken = token;
				break;
			}
		}
		return activeToken;
	}

	public boolean isVerified() {
		return isVerified == 1;
	}

	public void setVerified(boolean verified) {
		isVerified = verified==true ? 1: 0;
	}

	public void setActiveSession(UserSession session) {
		for (AuthorizationToken token : getSessions()) {
			if (token.getToken().equals(session.getSessionToken())) {
				token.setLastUpdated(new Date());
			}
		}
	}

	public void removeExpiredSessions(Date expiryDate) {
		for (AuthorizationToken token : getSessions()) {
			if (token.getLastUpdated().before(expiryDate)) {
				removeSession(token);
			}
		}
	}

	/**
	 * Hash the password using salt values See
	 * https://www.owasp.org/index.php/Hashing_Java
	 * 
	 * @param passwordToHash
	 * @return hashed password
	 */
	public String hashPassword(String passwordToHash) throws Exception {
		return hashToken(passwordToHash, getUuid().toString() + HASH_SALT);
	}

	private String hashToken(String token, String salt) throws Exception {

		return HashString.convertStringToHashedReprensentation(token ,salt.getBytes(), HASH_ITERATIONS);

	}

}
