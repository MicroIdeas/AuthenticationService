package ar.com.leadprogrammer.framework.user.api.model.social;

public class AccessGrant {

	private String accessToken;
	
	public AccessGrant(String accessToken) {
		this.accessToken= accessToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

}
