package ar.com.leadprogrammer.framework.user.api.model;

import javax.xml.bind.annotation.XmlRootElement;

import ar.com.leadprogrammer.framework.user.domain.entities.Location;

@XmlRootElement(name="location")
public class ExternalLocation {

	private Double latitude;

	private Double longitude;

	private String address;

	public ExternalLocation() {
	}

	public ExternalLocation(Location location) {
		if (location != null) {
			this.address = location.getAddress();
			this.longitude = location.getLongitude();
			this.latitude = location.getLatitude();
		}
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
