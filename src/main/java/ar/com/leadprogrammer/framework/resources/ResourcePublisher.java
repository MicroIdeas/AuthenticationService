package ar.com.leadprogrammer.framework.resources;

import java.io.Serializable;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * This class uses CDI to alias Java EE resources, such as the persistence
 * context, to CDI beans
 * 
 * <p>
 * Example injection on a managed bean field:
 * </p>
 * 
 * <pre>
 * &#064;Inject
 * private EntityManager em;
 * </pre>
 */
public class ResourcePublisher implements Serializable {
	private static final long serialVersionUID = -1791021278340800896L;

	private static final String JTA_PU = "container-managed";
	private static final Logger logger = Logger
			.getLogger(ResourcePublisher.class.getName());

	//@Produces
	@PersistenceContext(unitName = JTA_PU)
	private EntityManager em;

	//@Produces
//	public Logger produceLog(InjectionPoint injectionPoint) {
//		return produceLoggerUsingTheTargetClassName(injectionPoint);
//	}

	/*
	 * Internals Methods
	 */
//	private Logger produceLoggerUsingTheTargetClassName(
//			InjectionPoint injectionPoint) {
//
//        String className= injectionPoint.getMember().getDeclaringClass()
//                .getName();
//
//        logger.info("**Creating new Logger for class {} **".replace("{}",className));
//
//        return Logger.getLogger(className);
//	}



}
