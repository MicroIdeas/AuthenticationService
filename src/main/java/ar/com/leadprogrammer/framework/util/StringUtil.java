package ar.com.leadprogrammer.framework.util;


import java.util.regex.Pattern;

import org.junit.Assert;


/**
 * Author: Iain porter
 */
public class StringUtil {

    private static final Pattern UUID_PATTERN = Pattern.compile("^[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}$");

	public static void minLength(String str, int len) throws IllegalArgumentException {
		hasText(str);
		if (str.length() < len) {
			throw new IllegalArgumentException();
		}
	}

	private static void hasText(String str) {
		if (str==null || str.trim().isEmpty()){
			Assert.assertTrue(true);
		}
		
	}

	public static void maxLength(String str, int len) throws IllegalArgumentException {
		hasText(str);
		if (str.length() > len) {
			throw new IllegalArgumentException();
		}
	}

	public static void validEmail(String email) throws IllegalArgumentException {
		minLength(email, 4);
        maxLength(email, 255);
		if (!email.contains("@") || containsWhitespace(email)) {
			throw new IllegalArgumentException();
		}
	}

    private static boolean containsWhitespace(String email) {
    	//TODO
		return false;
	}

	public static boolean isValidUuid(String uuid) {
        return UUID_PATTERN.matcher(uuid).matches();
    }

	public static boolean hasLength(String str) {
		if (str==null || str.trim().isEmpty()){
			return false;
		}
		return true;
	}

}
