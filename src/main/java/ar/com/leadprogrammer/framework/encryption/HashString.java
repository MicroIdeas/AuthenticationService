package ar.com.leadprogrammer.framework.encryption;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashString {

	public static String convertStringToHashedReprensentation(String request) {
		MessageDigest md = null;
		byte[] digest;

		try {
			md = MessageDigest.getInstance("SHA-256");
			digest = md.digest(request.getBytes());

		} catch (NoSuchAlgorithmException e) {
			digest = request.getBytes();
		}

		return Base64Converter.convertStringToBase64Representation(digest);
	}

	public static String convertStringToHashedReprensentation(String password, byte[] salt, int numberOfIterations) throws Exception {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		digest.reset();
		digest.update(salt);
		byte[] input = digest.digest(password.getBytes("UTF-8"));
		for (int i = 0; i < numberOfIterations; i++) {
			digest.reset();
			input = digest.digest(input);
		}
		return Base64Converter.convertStringToBase64Representation(input);
	}

}
