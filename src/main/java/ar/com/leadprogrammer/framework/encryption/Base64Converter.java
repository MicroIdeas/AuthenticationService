package ar.com.leadprogrammer.framework.encryption;

import javax.xml.bind.DatatypeConverter;

public class Base64Converter {

	public static String convertStringToBase64Representation(String data) {

		return DatatypeConverter.printBase64Binary(data.getBytes());

	}
	
	public static String convertStringToBase64Representation(byte[] data) {

		return DatatypeConverter.printBase64Binary(data);
	}


	public static String convertBase64RepresentationToString(String data) {

		return new String(DatatypeConverter.parseBase64Binary(data));

	}


}
