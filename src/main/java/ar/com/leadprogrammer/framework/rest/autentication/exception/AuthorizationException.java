package ar.com.leadprogrammer.framework.rest.autentication.exception;



/**
 * User: porter
 * Date: 04/04/2012
 * Time: 15:32
 */
public class AuthorizationException extends BaseWebApplicationException {

	private static final long serialVersionUID = 956475576337824533L;

	public AuthorizationException(String applicationMessage) {
        super(403, "40301", "Not authorized", applicationMessage);
    }

}
