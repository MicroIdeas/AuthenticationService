package ar.com.leadprogrammer.framework.rest.autentication.exception;


/**
 *
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 14/09/2012
 */
public class AlreadyVerifiedException extends BaseWebApplicationException {

	private static final long serialVersionUID = 5670909822897269528L;

	public AlreadyVerifiedException() {
        super(409, "40905", "Already verified", "The token has already been verified");
    }
}
