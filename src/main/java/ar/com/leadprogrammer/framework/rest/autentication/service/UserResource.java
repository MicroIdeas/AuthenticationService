package ar.com.leadprogrammer.framework.rest.autentication.service;



import java.net.URI;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.com.leadprogrammer.framework.configuration.ApplicationConfig;
import ar.com.leadprogrammer.framework.rest.autentication.exception.AuthorizationException;
import ar.com.leadprogrammer.framework.user.api.connection.Connection;
import ar.com.leadprogrammer.framework.user.api.connection.ConnectionFactoryLocator;
import ar.com.leadprogrammer.framework.user.api.connection.OAuth2ConnectionFactory;
import ar.com.leadprogrammer.framework.user.api.model.AuthenticatedUserToken;
import ar.com.leadprogrammer.framework.user.api.model.ExternalUser;
import ar.com.leadprogrammer.framework.user.api.model.Role;
import ar.com.leadprogrammer.framework.user.api.model.social.AccessGrant;
import ar.com.leadprogrammer.framework.user.api.operations.CreateUserRequest;
import ar.com.leadprogrammer.framework.user.api.operations.LoginRequest;
import ar.com.leadprogrammer.framework.user.api.operations.OAuth2Request;
import ar.com.leadprogrammer.framework.user.api.operations.UpdateUserRequest;
import ar.com.leadprogrammer.framework.user.domain.service.EmailServicesGateway;
import ar.com.leadprogrammer.framework.user.domain.service.UserService;
import ar.com.leadprogrammer.framework.user.domain.service.VerificationTokenService;
import ar.com.leadprogrammer.framework.util.StringUtil;

/**
 * User: erodrig
 * Date: 12/03/2014
 */
@Path("/user")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@Component
public class UserResource {


	@Autowired
    protected UserService userService;

	@Autowired
    protected VerificationTokenService verificationTokenService;

	@Autowired
    protected EmailServicesGateway emailServicesGateway;

    @Context
    protected UriInfo uriInfo;

	@Autowired
    ApplicationConfig config;
    
	ConnectionFactoryLocator connectionFactoryLocator = new ConnectionFactoryLocator(); //this should recieve configuration

    @PermitAll
    @POST
    public Response signupUser(CreateUserRequest request) {
        AuthenticatedUserToken token = userService.createUser(request, Role.authenticated);
        verificationTokenService.sendEmailRegistrationToken(token.getUserId());
        URI location = uriInfo.getAbsolutePathBuilder().path(token.getUserId()).build();
        return Response.created(location).entity(token).build();
    }

    @RolesAllowed("admin")
    @Path("{userId}")
    @DELETE
    public Response deleteUser(@Context SecurityContext sc, @PathParam("userId") String userId) {
        ExternalUser userMakingRequest = (ExternalUser)sc.getUserPrincipal();
        userService.deleteUser(userMakingRequest, userId);
        return Response.ok().build();
    }

    @PermitAll
    @Path("login")
    @POST
    public Response login(LoginRequest request) {
        AuthenticatedUserToken token = userService.login(request);
        return getLoginResponse(token);
    }

    @PermitAll
    @Path("login/{providerId}")
    @POST
    public Response socialLogin(@PathParam("providerId") String providerId, OAuth2Request request) {
        OAuth2ConnectionFactory<?> connectionFactory = (OAuth2ConnectionFactory<?>) connectionFactoryLocator.getConnectionFactory(providerId);
        Connection<?> connection = connectionFactory.createConnection(new AccessGrant(request.getAccessToken()));
        AuthenticatedUserToken token = userService.socialLogin(connection);
        return getLoginResponse(token);
    }

    
    @RolesAllowed("admin")
	@GET
	public Response listAllUsers() {
		List<ExternalUser> retorno = userService.queryFindAll();
		
		
		return Response.ok().entity(new MakeUserRootNode(retorno)).build();
	}

    
    
    @RolesAllowed({"authenticated"})
    @Path("{userId}")
    @GET
    public Response getUser(@Context SecurityContext sc, @PathParam("userId") String userId) {
        ExternalUser userMakingRequest = (ExternalUser)sc.getUserPrincipal();
        ExternalUser user =  userService.getUser(userMakingRequest, userId);
        return Response.ok().entity(new MakeSingleUserRootNode(user)).build();
    }

    @RolesAllowed({"authenticated"})
    @Path("{userId}")
    @PUT
    public Response updateUser(@Context SecurityContext sc, @PathParam("userId") String userId, UpdateUserRequest request) {
        ExternalUser userMakingRequest = (ExternalUser)sc.getUserPrincipal();
        if(!userMakingRequest.getId().equals(userId)) {
            throw new AuthorizationException("User not authorized to modify this profile");
        }
        boolean sendVerificationToken = StringUtil.hasLength(request.getEmailAddress()) &&
                !request.getEmailAddress().equals(userMakingRequest.getEmailAddress());
        ExternalUser savedUser = userService.saveUser(userId, request);
        if(sendVerificationToken) {
            verificationTokenService.sendEmailVerificationToken(savedUser.getId());
        }
        return Response.ok().build();
    }

    private Response getLoginResponse(AuthenticatedUserToken token) {
        URI location = UriBuilder.fromPath(uriInfo.getBaseUri() + "user/" + token.getUserId()).build();
        return Response.ok().entity(token).contentLocation(location).build();
    }
    
    
    
	class MakeSingleUserRootNode{
		ExternalUser user;

		public MakeSingleUserRootNode(ExternalUser retorno) {
			this.user = retorno;
		}

		public ExternalUser getUser() {
			return user;
		}

		public void setUser(ExternalUser user) {
			this.user = user;
		}
		
	}

	
	class MakeUserRootNode{
		List<ExternalUser> user;

		public MakeUserRootNode(List<ExternalUser> retorno) {
			this.user = retorno;
		}

		public List<ExternalUser> getUser() {
			return user;
		}

		public void setUser(List<ExternalUser> user) {
			this.user = user;
		}
		
	}


}
