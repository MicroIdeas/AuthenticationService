package ar.com.leadprogrammer.framework.rest.autentication.filter;

import java.io.IOException;
import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.springframework.beans.factory.annotation.Autowired;

import ar.com.leadprogrammer.framework.configuration.ApplicationConfig;
import ar.com.leadprogrammer.framework.rest.autentication.AuthorizationRequestContext;
import ar.com.leadprogrammer.framework.rest.autentication.AuthorizationService;
import ar.com.leadprogrammer.framework.rest.autentication.impl.RequestSigningAuthorizationService;
import ar.com.leadprogrammer.framework.rest.autentication.impl.SecurityContextImpl;
import ar.com.leadprogrammer.framework.rest.autentication.impl.SessionTokenAuthorizationService;
import ar.com.leadprogrammer.framework.user.api.model.ExternalUser;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class SecurityContextFilter implements ContainerRequestFilter {
	protected static final String HEADER_AUTHORIZATION = "Authorization";
	protected static final String HEADER_DATE = "x-java-rest-date";
	protected static final String HEADER_NONCE = "nonce";

	private static final Logger logger = Logger
			.getLogger(SecurityContextFilter.class.getName());

	protected static final ApplicationConfig applicationConfig = new ApplicationConfig();

	private AuthorizationService authorizationService;

	@Autowired
	public SecurityContextFilter(RequestSigningAuthorizationService requestSigningAuthorizationService, SessionTokenAuthorizationService stas ) {
		if (applicationConfig != null) {
			if (applicationConfig.requireSignedRequests()) {
				this.authorizationService = requestSigningAuthorizationService;
			} else {
				this.authorizationService = stas;
			}
		}
	}

	@Override
	public void filter(ContainerRequestContext requestContext)
			throws IOException {

		logger.info("Entering JAX-RS Conainer Filter");
		if (applicationConfig != null) {

			requestContext.getHeaders();

			String authToken = requestContext
					.getHeaderString(HEADER_AUTHORIZATION);
			String requestDateString = requestContext
					.getHeaderString(HEADER_DATE);
			String nonce = requestContext.getHeaderString(HEADER_NONCE);
			AuthorizationRequestContext context = new AuthorizationRequestContext(
					requestContext.getUriInfo().getPath(),
					requestContext.getMethod(), requestDateString, nonce,
					authToken);
			ExternalUser externalUser = authorizeUserUsingService(context);
			requestContext.setSecurityContext(new SecurityContextImpl(
					externalUser));

		}

	}

	private ExternalUser authorizeUserUsingService(
			AuthorizationRequestContext context) {
		ExternalUser externalUser = null;
		externalUser = authorizationService.authorize(context);
		return externalUser;
	}
}