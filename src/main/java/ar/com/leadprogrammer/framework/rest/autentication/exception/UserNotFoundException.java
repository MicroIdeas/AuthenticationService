package ar.com.leadprogrammer.framework.rest.autentication.exception;


/**
 *
 * @version 1.0
 * @author: Iain Porter iain.porter@porterhead.com
 * @since 12/09/2012
 */
public class UserNotFoundException extends BaseWebApplicationException {

	private static final long serialVersionUID = 1917842901152933579L;

	public UserNotFoundException() {
        super(404, "40402", "User Not Found", "No User could be found for that Id");
    }
}
