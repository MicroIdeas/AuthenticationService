package ar.com.leadprogrammer.framework.rest.autentication.impl;

import java.util.Date;

import javax.annotation.ManagedBean;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ar.com.leadprogrammer.framework.rest.autentication.AuthorizationRequestContext;
import ar.com.leadprogrammer.framework.rest.autentication.AuthorizationService;
import ar.com.leadprogrammer.framework.rest.autentication.exception.AuthorizationException;
import ar.com.leadprogrammer.framework.user.api.model.ExternalUser;
import ar.com.leadprogrammer.framework.user.domain.entities.AuthorizationToken;
import ar.com.leadprogrammer.framework.user.domain.entities.User;
import ar.com.leadprogrammer.framework.user.domain.service.UserService;

/**
 *
 * Simple authorization service that requires a session token in the Authorization header
 * This is then matched to a user
 *
 * @version 1.0
 * @author: Iain Porter
 * @since 29/01/2013
 */
@Component
public class SessionTokenAuthorizationService implements AuthorizationService {

    /**
     * directly access user objects
     */

	@Autowired
	private UserService userService;


    public SessionTokenAuthorizationService() {
		super();
    }


	public ExternalUser authorize(AuthorizationRequestContext securityContext) {
    	
    	
    	
        String token = securityContext.getAuthorizationToken();
        ExternalUser externalUser = null;
        if(token == null) {
            return externalUser;
        }
        User user =  userService.findBySession(token);
        if(user == null) {
            throw new AuthorizationException("Session token not valid");
        }
        for (AuthorizationToken sessionToken : user.getSessions()) {
            if (sessionToken.getToken().equals(token)) {
                sessionToken.setLastUpdated(new Date());
                userService.save(user);
                externalUser = new ExternalUser(user);
            }
        }
        return externalUser;
    }

}
