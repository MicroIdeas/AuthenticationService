package ar.com.leadprogrammer.framework.configuration;

import javax.annotation.ManagedBean;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Named;

import org.springframework.stereotype.Component;

@ManagedBean
@Component
public class ApplicationConfig {
	
	public ApplicationConfig(){
		super();
	}

	public int getSessionDateOffsetInMinutes() {
		return 30;
	}

	public int getEmailVerificationTokenExpiryTimeInMinutes() {
		return 0;
	}

	public String getHostNameUrl() {
		return null;
	}

	public int getEmailRegistrationTokenExpiryTimeInMinutes() {
		return 0;
	}

	public int getLostPasswordTokenExpiryTimeInMinutes() {
		return 0;
	}

	public boolean requireSignedRequests() {
		return false;
	}

}
