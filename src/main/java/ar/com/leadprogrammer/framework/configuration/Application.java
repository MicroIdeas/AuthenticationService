package ar.com.leadprogrammer.framework.configuration;

import java.util.Arrays;

import javax.ws.rs.ext.Provider;
import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ar.com.leadprogrammer.framework.rest.autentication.filter.SecurityContextFilter;
import ar.com.leadprogrammer.framework.rest.autentication.service.PasswordResource;
import ar.com.leadprogrammer.framework.rest.autentication.service.UserResource;
import ar.com.leadprogrammer.framework.rest.autentication.service.VerificationResource;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@SpringBootApplication
@ComponentScan(value={"ar.com.leadprogrammer.framework","co.microideas.framework"}, includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Provider.class))
@EntityScan( basePackages={"ar.com.leadprogrammer.framework.user.domain.entities"})
@EnableJpaRepositories(basePackages = {"ar.com.leadprogrammer.framework.user.domain.service"})
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableDiscoveryClient
@EnableConfigurationProperties

public class Application {
	
	@Configuration
  @ApplicationPath("/services")
    public static class JerseyConfig extends ResourceConfig {

        public JerseyConfig() {
           this.register(UserResource.class);
           this.register(VerificationResource.class);
           this.register(PasswordResource.class);
           this.register(SecurityContextFilter.class);
           this.register(RolesAllowedDynamicFeature.class);

        }
    }

    public static void main(String[] args) {
        ApplicationContext ctx = new SpringApplicationBuilder(Application.class).showBanner(false).run(args);

    }

}