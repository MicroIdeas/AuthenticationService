package ar.com.leadprogrammer.test.encryption;

import junit.framework.Assert;

import org.junit.Test;

import ar.com.leadprogrammer.framework.encryption.Base64Converter;

public class Base64ConverterTest {

	@Test
	public void encodeAString() {

		String resultingChain = Base64Converter
				.convertStringToBase64Representation("SomeIdentifier");
		Assert.assertEquals("U29tZUlkZW50aWZpZXI=", resultingChain);
	}

	@Test
	public void decodeAString_ok() {
		String codifiedString = "U29tZUlkZW50aWZpZXI=";
		String remasteredString = Base64Converter
				.convertBase64RepresentationToString(codifiedString);
		Assert.assertEquals("SomeIdentifier", remasteredString);

	}

	@Test
	public void encodeAByteArray() {
		byte[] someData = "SomeIdentifier".getBytes();
		
		String resultingChain = Base64Converter
				.convertStringToBase64Representation(someData);
		Assert.assertEquals("U29tZUlkZW50aWZpZXI=", resultingChain);
	}

}
