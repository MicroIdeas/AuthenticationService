package ar.com.leadprogrammer.test.annotations;



import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;

import ar.com.leadprogrammer.framework.validations.annotations.EmailValidator;



/**
 * @author Hardy Ferentschik
 */
public class EmailValidatorTest {
	private static EmailValidator validator;


	@BeforeClass
	public static void init() {
		validator = new EmailValidator();
	}

	@Test
	public void testNullAndEmptyString() throws Exception {
		isValidEmail( "" );
		isValidEmail( null );
	}

	@Test
	public void testValidEmail() throws Exception {
		isValidEmail( "emmanuel@hibernate.org" );
		isValidEmail( "emmanuel@hibernate" );
		isValidEmail( "emma-n_uel@hibernate" );
		isValidEmail( "emma+nuel@hibernate.org" );
		isValidEmail( "emma=nuel@hibernate.org" );
		isValidEmail( "emmanuel@[123.12.2.11]" );
		isValidEmail( "*@example.net" );
		isValidEmail( "fred&barny@example.com" );
		isValidEmail( "---@example.com" );
		isValidEmail( "foo-bar@example.net" );
		isValidEmail( "mailbox.sub1.sub2@this-domain" );
	}

	@Test
	public void testInValidEmail() throws Exception {
		isInvalidEmail( "emmanuel.hibernate.org" );
		isInvalidEmail( "emma nuel@hibernate.org" );
		isInvalidEmail( "emma(nuel@hibernate.org" );
		isInvalidEmail( "emmanuel@" );
		isInvalidEmail( "emma\nnuel@hibernate.org" );
		isInvalidEmail( "emma@nuel@hibernate.org" );
		isInvalidEmail( "Just a string" );
		isInvalidEmail( "string" );
		isInvalidEmail( "me@" );
		isInvalidEmail( "@example.com" );
		isInvalidEmail( "me.@example.com" );
		isInvalidEmail( ".me@example.com" );
		isInvalidEmail( "me@example..com" );
		isInvalidEmail( "me\\@example.com" );
	}

	@Test
	public void testAccent() {
		isValidEmail( "Test^Email@example.com" );
	}

	@Test
	public void testValidEmailCharSequence() throws Exception {
		isValidEmail(  "emmanuel@hibernate.org"  );
		isInvalidEmail(  "@example.com"  );
	}

	@Test
	public void testMailWithInternationalDomainName() throws Exception {
		isValidEmail( "myname@Ã¶stereich.at", "A valid email address with umlaut" );
		isInvalidEmail( "Î¸ÏƒÎµÏ.ÎµÏ‡Î±Î¼Ï€Î»Îµ.ÏˆÎ¿Î¼", "Email does not contain an @ character and should be invalid" );
	}

	@Test
	public void testEmailAddressLength() {
		isValidEmail( "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa@hibernate.org" );
	}

	@Test
	public void testEMailWithTrailingAt() throws Exception {
		isInvalidEmail( "validation@hibernate.com@" );
		isInvalidEmail( "validation@hibernate.com@@" );
		isInvalidEmail( "validation@hibernate.com@@@" );
	}



	private void isValidEmail(CharSequence email, String message) {
		assertTrue( validator.isValid( email, null ), message );
	}

	private void assertTrue(boolean valid, String message) {
		Assert.assertTrue(message,valid);		
		
	}

	private void isValidEmail(CharSequence email) {
		isValidEmail( email, "Expected a valid email." );
	}

	private void isInvalidEmail(CharSequence email, String message) {
		assertFalse( validator.isValid( email, null ), message );
	}

	private void assertFalse(boolean valid, String message) {
		Assert.assertFalse(message,valid);		
	}

	private void isInvalidEmail(CharSequence email) {
		isInvalidEmail( email, "Expected a invalid email." );
	}

	@SuppressWarnings("unused")
	private abstract static class EmailContainer {
		public String email;

		public void setEmail(String email) {
			this.email = email;
		}

		public String getEmail() {
			return email;
		}
	}

}