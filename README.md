Authentication Service for Microservice Architecture
=========================

This project will handle web Oath2 autentication, that will enable login, 
and will allow social media network login integration, like gmail and facebook



Initial implementation
--------------------

Receives an user name and passsowd and will return a api key that can be used to talk with other services for 
the remaining of the session duration

Allow the creation of an user and will return the user identifier

Allow two kind of login mail or socialmedia access, agregatting the information from both sources

Allow mail verification, sending a mail validation with a particular url


GET 
http://localhost:8080/user/ 

{
    "user": [
        {
            "emailAddress": "admin@admin.com",
            "firstName": "System",
            "id": "ab781ea7-594a-443f-8f6f-f21e88ef6c66",
            "lastName": "Administrator",
            "name": "admin@admin.com",
            "role": "administrator",
            "socialProfiles": [],
            "verified": true
        },
        {
            "emailAddress": "sinapsis@gmail.com",
            "firstName": "Eduardo",
            "id": "ab781ea7-594a-443f-8f6f-f21e88ef6c67",
            "lastName": "Rodriguez",
            "name": "sinapsis@gmail.com",
            "role": "administrator",
            "socialProfiles": [],
            "verified": true
        },
        {
            "emailAddress": "johndoe@gmail.com",
            "firstName": "John",
            "id": "ab781ea7-594a-443f-8f6f-f21e88ef6c69",
            "lastName": "Doe",
            "name": "johndoe@gmail.com",
            "role": "authenticated",
            "socialProfiles": [],
            "verified": true
        }
    ]
}



POST
http://localhost:8080/user/ 
{"user":{"firstName":"Eduardo","lastName":"Rodriguez","emailAddress":"sinapsis490@gmail.com"},"password":"welcome1"}




